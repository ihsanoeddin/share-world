class CreateManifests < ActiveRecord::Migration
  def change
    create_table :manifests do |t|
      t.references :activity, index: true
      t.references :identity, index: true
      t.integer :state

      t.timestamps null: false
    end
    #add_foreign_key :manifests, :activities
    #add_foreign_key :manifests, :identities
  end
end
