class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.references :activity, index: true
      t.string :url
      t.string :callback_url
      t.string :image
      t.timestamps null: false
    end
    #add_foreign_key :links, :activities
  end
end
