class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true
      t.date :date_of_birth
      t.string :mobile, limit: 20
      t.string :address, limit: 100
      t.string :city, limit: 50
      t.string :province, limit: 50
      t.text :description

      t.timestamps null: false
    end
    #add_foreign_key :profiles, :users
  end
end
