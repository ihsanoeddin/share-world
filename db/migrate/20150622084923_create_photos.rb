class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.references :activity, index: true
      t.string :name
      t.text :description
      t.timestamps null: false
    end
    #add_foreign_key :photos, :activities
  end
end
