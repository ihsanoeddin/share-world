module ApplicationHelper

  def new_activity_manifest activity
    activity.manifests.build
  end

  def alert_class level
    style = 'default'
    case level
      when :notice
        style = 'info'
      when :error
        style = 'danger'
      when :alert
        style = 'warning'
      when :success
        style = 'success'
    end
    style
  end

  def avatar user=nil
    if profile user
      return profile(user).avatar.blank? ? default_avatar : profile(user).avatar
    end
    default_avatar
  end

  def profile user = nil
    user.nil? ? current_user.profile : user.profile
  end

  def default_avatar
    'avatar-default.png'
  end

  def search_route route = nil
    if route.nil?
      case params[:controller]
        when 'profile' then people_path
        when 'activities' then activities_path
        else route
      end
    end
    route
  end

  def activity_attachment activity

  end

  def authored_by user
    user.id.eql?(current_user.id) ? 'you' : link_to(user.name, '#')
  end

end
