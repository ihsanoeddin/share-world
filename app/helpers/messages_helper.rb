module MessagesHelper

  def unread_messages_count
    mailbox.inbox(:unread => true).count(:id, :distinct => true)
  end

  def active_folder folder
    "active" if params[:action].eql? folder
  end

end

