class User < ActiveRecord::Base

  extend FriendlyId

  has_many :identities
  has_many :activities
  has_one :profile, dependent: :destroy
  accepts_nested_attributes_for :profile, allow_destroy: true

  acts_as_followable
  acts_as_follower

  acts_as_messageable

  TEMP_EMAIL_PREFIX = 'dummy@mail'
  TEMP_EMAIL_REGEX = /\Adummy@mail/

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :omniauthable

  friendly_id :slug_candidates, use: :slugged

  validates_presence_of :name
  validates :email, presence: true, uniqueness: true, format: { without: TEMP_EMAIL_REGEX, on: :update }
  with_options :if => :password_required? do |v|
    v.validates_presence_of     :password
    v.validates_confirmation_of :password
    v.validates_length_of       :password, :within => Devise.password_length, :allow_blank => true
  end

  attr_accessor :status

  class << self

    def find_for_oauth(auth, signed_in_resource = nil)
      identity = Identity.find_for_oauth(auth)
      user = signed_in_resource ? signed_in_resource : identity.user
      if user.nil?
        email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
        email = auth.info.email if email_is_verified
        user = User.where(:email => email).first if email
        if user.nil?
          user = User.new(
            name: auth.extra.raw_info.name,
            email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
            password: Devise.friendly_token[0,20]
          )
          user.skip_confirmation!
          user.save!
        end
      end
      if identity.user != user
        identity.user = user
        identity.save!
      end
      user
    end

    def search params
      if params.is_a? Array
        where(id: 0)
      else
        where('lower(name) like ?', "%#{params.to_s.downcase}%")
      end
    end

  end

  #friendly slug candidates
  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end

  #mailboxer methods
  def mailboxer_email object
    email
  end

  def mailboxer_name
    name
  end

  #activities
  def recent_activities search = nil, page=nil,per=10
    activities.where('lower(title) like ?', "%#{search}%").order('updated_at desc').page(page).per(per) + followees_recent_activities(page,per)
  end

  def followees_recent_activities search=nil, page=nil,per=10
    Activity.where(user_id: followees(self.class).map(&:id)<<self.id).where('lower(title) like ?', "%#{search}%").order('updated_at desc').order('updated_at desc').page(page).per(per)
  end

  protected

  # From devise
  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

end
