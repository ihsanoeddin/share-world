class Link < ActiveRecord::Base

  belongs_to :activity

  validates :url, presence: true
end
