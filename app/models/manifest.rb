class Manifest < ActiveRecord::Base
  belongs_to :activity
  belongs_to :identity

  accepts_nested_attributes_for :activity

  attr_accessor :publish_on
end
