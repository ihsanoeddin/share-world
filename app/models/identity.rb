class Identity < ActiveRecord::Base
  belongs_to :user
  has_many :manifests
  has_many :activities, through: :manifests
  accepts_nested_attributes_for :manifests

  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider

  class << self
	  def find_for_oauth(auth)
	    find_or_create_by(uid: auth.uid, provider: auth.provider)
	  end
  end
end
