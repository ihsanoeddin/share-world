class Activity < ActiveRecord::Base

  extend FriendlyId

  belongs_to :user
  has_one :link
  has_one :photo
  has_one :post
  has_many :manifests
  has_many :identities, through: :manifests
  accepts_nested_attributes_for :manifests

  friendly_id :title, :use => :slugged

  validates_presence_of :title

  before_save :content

  attr_accessor :image, :image_description, :video, :video_description, :status

  validate :share

  class << self
  end

  def create_activity
    manifests.each do |manifest| manifests.delete(manifest) unless manifest.publish_on end
    save
  end

  def image?
    image.instance_of? ActionDispatch::Http::UploadedFile
  end

  def link?
    /(https?:\/\/)?\w*\.\w+(\.\w+)*(\/\w+)*(\.\w*)?/.match(title)
  end

  def content
    begin
      if image?
        build_photo name: image
      elsif link?
        url = link?.to_s
        build_link(url: url)
      else
        build_post content: title
      end
    rescue e
      return false
    end
  end

  def share

  end

end
