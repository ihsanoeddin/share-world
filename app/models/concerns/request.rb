module Request
  extend ActiveSupport::Concern
  class << self
  end

  def request(http_method, url, options = {})
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = options[:use_ssl] || false
    header = options[:header] || {'Content-Type' =>'application/json'}
    begin
      http_method =  "Net::HTTP::#{http_method.capitalize}".constantize
      request = http_method.new(uri.request_uri, header)
      request.body = options[:data].to_json if options[:data].present?
      response = http.request(request)
      if http_method.eql?(Net::HTTP::Put) && response.is_a?(Net::HTTPOK)
        obj = options[:data].to_json
      else
        obj = JSON.parse(response.body) rescue nil
      end
    rescue => e
      obj = { 'errors' =>  ['connection timed out']}
    end
    obj
  end

end