class ProfilesController < BaseController

  @@use_resource = false
  before_filter :profile, only: [:edit]

  def show

  end

  def edit

  end

  def update
    if current_user.send update_with_password?, appropriate_params
      sign_in(current_user, :bypass => true) if update_password?
      respond_to do |f|
        f.html{redirect_to profile_path, notice: 'Profile updated.'}
        f.js{}
      end
    else
      respond_to do |f|
        f.html{ render update_password? ? :edit_password : :edit}
        f.js{}
      end
    end
  end

  def edit_password

  end

  protected

  def user_params
    params.require(:user).permit(:name, profile_attributes: [:date_of_birth, :avatar, :mobile, :address, :city, :province])
  end

  def password_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end

  def appropriate_params
    update_password? ? password_params : user_params
  end

  def profile
    if current_user.profile.blank?
      current_user.build_profile
    end
  end

  def update_password?
    !password_params.empty?
  end

  def update_with_password?
    update_password? ? :update_with_password : :update_without_password
  end

end
