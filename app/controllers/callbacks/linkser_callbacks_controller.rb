class Callbacks::LinkserCallbacksController < ApplicationController

  def get
    if params[:url].present?
      begin
          thumb = LinkThumbnailer.generate(params[:url], redirect_limit: 5, user_agent: browser.user_agent)
          if thumb.is_a? LinkThumbnailer
            render "preview", {thumb: thumb}
            return
          end
        rescue
          render "error", {message: "Error message"}
          return
        end
      end
      render "error", {message: "Error message"}
    end
  end

end