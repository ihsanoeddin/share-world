class ActivitiesController < BaseController

  @@friendly_id = true

  before_filter :activity, only: [:create]
  respond_to :js

  def index

  end

  def show

  end

  def create
    @resource.create_activity
    redirect_to root_path, notice: 'Post submitted'
  end

  def update

  end

  protected

  private

  def activity
    @resource.user = current_user
  end

  def activity_params
    params.require(:activity).permit(:title, :image, manifests_attributes: [:identity_id, :publish_on])
  end

end
