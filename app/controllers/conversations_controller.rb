class ConversationsController < BaseController
  @@use_resource = false
  before_filter :receipts, only: [:show]

  def new
  end

  def create
    create_mailboxer
    if @mailboxer.valid?
      respond_to do |f|
        f.html{ redirect_to conversation_path @mailboxer.conversation, success: "Your message was successfully sent!" }
        f.js{}
      end
    else
      respond_to do |f|
        f.html{ render :new }
        f.js{}
      end
    end
  end

  def show
    conversation.mark_as_read(current_user)
  end

  def reply
    reply_conversation
    if @mailboxer.valid?
      respond_to do |f|
        f.html {redirect_to conversation_path conversation, notice: "Your reply message was successfully sent!"}
      end
    else
      respond_to do |f|
        receipts
        f.html{render :show}
      end
    end
  end

  def trash
    conversation.move_to_trash(current_user)
    redirect_to messages_inbox_path
  end

  def untrash
    conversation.untrash(current_user)
    redirect_to messages_inbox_path
  end

  private

  def conversation_params
    params.require(:conversation).permit(:subject, :body,recipients:[])
  end

  def message_params
    params.require(:message).permit(:body, :subject)
  end

  def reply_subject
    message_params[:subject]
  end

  def recipients
    @recipients ||= User.where(id: conversation_params[:recipients])
  end

  def create_mailboxer
    @mailboxer ||= current_user.send_message(recipients, conversation_params[:body], conversation_params[:subject])
  end

  def receipts
    @receipts = conversation.receipts_for(current_user)
  end

  def reply_conversation
    @mailboxer ||= current_user.reply_to_conversation conversation, message_params[:body], reply_subject
  end

end
