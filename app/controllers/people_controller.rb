class PeopleController < BaseController

  @@model_resource = 'User'
  @@friendly_id = true

  before_action :set_resource, only: [:show, :follow, :unfollow]

  def index
    @people = resource_class_constant.search(params[:search]).where.not(id: current_user.id).page(params[:page]).per(10)
    respond_to do |f|
      f.html{}
      f.js{}
    end
  end

  def show

  end

  def follow
    current_user.follow!(resource)
    redirect_to people_path, notice: "You followed #{resource.name}"
  end

  def unfollow
    current_user.unfollow!(resource)
    redirect_to people_path, notice: "You unfollow #{resource.name}"
  end

end
