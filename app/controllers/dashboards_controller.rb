class DashboardsController < BaseController

  def index
    @recents_activities = current_user.followees_recent_activities(params[:search], params[:page],params[:per]).sort_by(&:updated_at).reverse
	end

end
