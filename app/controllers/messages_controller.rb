class MessagesController < BaseController

  def inbox
    @inbox = mailbox.inbox
  end

  def sent
    @sent = mailbox.sentbox
  end

  def trash
    @trash = mailbox.trash
  end

end
