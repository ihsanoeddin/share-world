module Resources
  extend ActiveSupport::Concern

  included do
    
    class_attribute :model_resource
    class_attribute :resources_action
    class_attribute :use_resource
    class_attribute :identifier
    class_attribute :friendly_id
    class_attribute :resource

    self.model_resource = nil
    self.resources_action = [:new, :create, :edit, :update, :show]
    self.use_resource = true
    self.identifier = 'id'
    self.friendly_id = false

    prepend_before_action :set_resource, only: mine.resources_action
  end

  protected

  def identifier
    mine.identifier
  end

  def model_resource
    mine.model_resource
  end

  def use_resource?
    mine.use_resource
  end

  def friendly_identifier?
    mine.friendly_id
  end

  def resource
    @resource ||= set_resource
  end

  def set_resource
    if mine.use_resource
      identifier? ? existing_resource : new_resource
    end
  end

  def new_resource
    @resource = resource_class_constant.new resource_params
  end

  def existing_resource
    @resource = friendly_identifier? ? resource_class_constant.friendly.find(params[identifier.to_sym]) : resource_class_constant.send("find_by_#{identifier}")
    raise {ActiveRecord::RecordNotFound} if @resource.nil?
  end

  def identifier?
    params[identifier.to_sym].present?
  end

  def resource_params
    respond_to?("#{resource_class.underscore.downcase}_params", true) ? self.send("#{resource_class.underscore.downcase}_params") : {}
  end

  def class_exists?(class_name)
    klass = Module.const_get(class_name)
      return klass.is_a?(Class) && klass < ActiveRecord::Base
    rescue NameError
      return false
  end

  def resource_class
    resource_class = mine.model_resource
    if resource_class.nil?
      resource_class = params[:controller].singularize.capitalize
    end
    resource_class
  end

  def resource_class_constant
    class_exists?(resource_class) ? resource_class.constantize : raise {ActiveRecord::RecordNotFound}
  end

  def mine
    self.class
  end

end