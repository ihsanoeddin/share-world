module Authentication
  extend ActiveSupport::Concern
  included do
    before_filter :authenticate_user!
    before_action :configure_permitted_devise_parameters
  end

  protected

  def configure_permitted_devise_parameters
    if devise_controller?
      devise_parameter_sanitizer.for(:sign_up) << :name
    end
  end

end
