module  Mailbox
  extend ActiveSupport::Concern
  included do
    helper_method :mailbox, :conversation
  end

  protected

  def mailbox
    @mailbox ||= current_user.mailbox
  end

  def conversation
    @conversation ||= mailbox.conversations.find(params[:id])
  end

end
